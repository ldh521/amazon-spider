package cn.amazonspider.pipeline;

import cn.amazonspider.dao.AmazonDao;
import cn.amazonspider.model.AmazonItem;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.PageModelPipeline;

@Component
public class AmazonPageModelPipeline implements PageModelPipeline<AmazonItem> {
    private Logger logger = Logger.getLogger(getClass());
    private String table;
    private String first_sort;
    private String second_sort;
    @Autowired
    private AmazonDao amazonDao;
    @Override
    public void process(AmazonItem amazonItem, Task task) {
        amazonItem.setFirst_sort(this.first_sort);
        amazonItem.setSecond_sort(this.second_sort);
        logger.info("save AmazonItem:"+amazonItem.toString());
        amazonDao.save(amazonItem,this.table);
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getFirst_sort() {
        return first_sort;
    }

    public void setFirst_sort(String first_sort) {
        this.first_sort = first_sort;
    }

    public String getSecond_sort() {
        return second_sort;
    }

    public void setSecond_sort(String second_sort) {
        this.second_sort = second_sort;
    }
}

package cn.amazonspider.dao;

import cn.amazonspider.model.AmazonItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class AmazonDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 持久化
     *
     * @param amazonItem
     * @param table
     */
    public void save(AmazonItem amazonItem,String table){
        String sql = "insert into "
                +table
                +"(mid,mname,first_sort,second_sort,title,url,adwords,price,mprice,picture,comments,updated) value(?,?,?,?,?,?,?,?,?,?,?,?)";
        Object[] params = new Object[]{
                amazonItem.getMid(), amazonItem.getMname(), amazonItem.getFirst_sort(),
                amazonItem.getSecond_sort(), amazonItem.getTitle(),amazonItem.getUrl(),
                amazonItem.getAdwords(),amazonItem.getPrice(),amazonItem.getMprice(),
                amazonItem.getPicture(),amazonItem.getComments(),amazonItem.getUpdated()
        };
        jdbcTemplate.update(sql, params);
    }
}

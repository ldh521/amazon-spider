package cn.amazonspider.model;

/**
 * @author Mr_Tank_
 *
 * 用于分类、入库数据表等设置通用类
 */
public class CrawlerModel {
    private String seed;         //seed url
    private String table;        //Name of the database table
    private String first_sort;   //Commodity classification level:The primary classification
    private String second_sort;  //Commodity classification level:The secondary classification

    public CrawlerModel(String seed, String table, String first_sort, String second_sort) {
        this.seed = seed;
        this.table = table;
        this.first_sort = first_sort;
        this.second_sort = second_sort;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getFirst_sort() {
        return first_sort;
    }

    public void setFirst_sort(String first_sort) {
        this.first_sort = first_sort;
    }

    public String getSecond_sort() {
        return second_sort;
    }

    public void setSecond_sort(String second_sort) {
        this.second_sort = second_sort;
    }
}

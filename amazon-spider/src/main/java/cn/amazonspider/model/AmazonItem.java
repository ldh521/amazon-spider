package cn.amazonspider.model;

import cn.amazonspider.utils.Constants;
import cn.amazonspider.utils.GetDate;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.model.AfterExtractor;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.TargetUrl;


@TargetUrl("http://www.amazon.cn/s/ref=sr_pg_\\S+?rh=\\S+")
@ExtractBy(value="//div[@id='btfResults']/*",multi =true)
public class AmazonItem implements AfterExtractor {
    private int id;              //商品id
    private int mid;             //商城id
    private String mname;        //商城名称
    private String first_sort;   //一级分类
    private String second_sort;  //二级分类

    @ExtractBy(value="//span[@class='lrg']/text()",notNull = true)
    private String title;     //商品描述

    private String adwords;   //促销广告

    @ExtractBy("//span[@class='bld lrg red']/regex('\\d+')")
    private String price;     //商城价格

    @ExtractBy("//del[@class='grey']/regex('\\d+')")
    private String mprice;    //市场价格

    @ExtractBy("//img[@class='productImage']/@src")
    private String picture;   //图片URL

    @ExtractBy("//a[@class='longReview']/text()")
    private String comments;  //商城评论

    private String updated;   //更新时间

    @ExtractBy("//h3[@class='newaps']/a/@href")
    private String url;       //商品URL

    @Override
    public String toString() {
        return "AmazonItem[title:"+this.title
                +";adwords:"+this.adwords
                +";price:"+this.price
                +";comments:"+this.comments
                +";picture:"+this.picture
                +";url:" +this.url
                +"]"+"\n";
    }
    @Override
    public void afterProcess(Page page){
        this.mid= Constants.MALLNUM_AMAZOM;
        this.mname="亚马逊";
        this.updated= GetDate.getdate();
        this.comments=this.comments!=null?this.comments.replace("条评论","").trim():this.comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }



    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getFirst_sort() {
        return first_sort;
    }

    public void setFirst_sort(String first_sort) {
        this.first_sort = first_sort;
    }

    public String getSecond_sort() {
        return second_sort;
    }

    public void setSecond_sort(String second_sort) {
        this.second_sort = second_sort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAdwords() {
        return adwords;
    }

    public void setAdwords(String adwords) {
        this.adwords = adwords;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMprice() {
        return mprice;
    }

    public void setMprice(String mprice) {
        this.mprice = mprice;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
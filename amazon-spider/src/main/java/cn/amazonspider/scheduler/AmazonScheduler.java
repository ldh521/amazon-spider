package cn.amazonspider.scheduler;

import cn.amazonspider.crawler.AmazonCrawler;
import cn.amazonspider.model.CrawlerModel;
import cn.amazonspider.utils.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr_Tank_
 *
 */
@Component
public class AmazonScheduler{
    private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private AmazonCrawler amazonCrawler;

    @Scheduled(cron = "0/15 * * * * ?")//cron for test
    public void test(){
        logger.info("scheduler test.");
    }


    @Scheduled(cron = "0 28 16 ? * MON-FRI")
    public void amazonScheduler(){
        logger.info("Amazon Crawler run!");
        amazonCrawler.addCrawlerModel(getCrawlerModelList()).amazoncrawler();
    }

    /**
     * 准备URL等数据
     *
     * @return
     */
    private List<CrawlerModel> getCrawlerModelList() {
        logger.info("setCrawlerModelList.");
        List<CrawlerModel> crawlerModelList = new ArrayList<CrawlerModel>();
        CrawlerModel phone = new CrawlerModel(Constants.AMAZONSEED_PHONE, Constants.TABLE, Constants.SORT_PHONE, "手机通讯");
        CrawlerModel phoneac = new CrawlerModel(Constants.AMAZONSEED_PHONE_AC, Constants.TABLE, Constants.SORT_PHONE, "手机配件");

        CrawlerModel cammera = new CrawlerModel(Constants.AMAZONSEED_CAMERA, Constants.TABLE, Constants.SORT_PHONE, "数码相机");
        CrawlerModel dv = new CrawlerModel(Constants.AMAZONSEED_DV, Constants.TABLE, Constants.SORT_PHONE, "数码摄像机");
        CrawlerModel filmcammera = new CrawlerModel(Constants.AMAZONSEED_FILMCAMERA, Constants.TABLE, Constants.SORT_PHONE, "胶片相机");
        CrawlerModel lens = new CrawlerModel(Constants.AMAZONSEED_LENS, Constants.TABLE, Constants.SORT_PHONE, "镜头");
        CrawlerModel cammeraac = new CrawlerModel(Constants.AMAZONSEED_CAMERA_AC, Constants.TABLE, Constants.SORT_PHONE, "配件");
        CrawlerModel digi = new CrawlerModel(Constants.AMAZONSEED_DIDG, Constants.TABLE, Constants.SORT_PHONE, "数码影音");
        CrawlerModel computer = new CrawlerModel(Constants.AMAZONSEED_COMPURER, Constants.TABLE, Constants.SORT_COMPUTER, "电脑整机");
        CrawlerModel tv = new CrawlerModel(Constants.AMAZONSEED_TV, Constants.TABLE, Constants.SORT_APPLIANCES, "平板电视");
        CrawlerModel appliance_b = new CrawlerModel(Constants.AMAZONSEED_APPLIANCES_B, Constants.TABLE, Constants.SORT_APPLIANCES, "大家电");
        CrawlerModel appliance_s = new CrawlerModel(Constants.AMAZONSEED_APPLIANCES_S, Constants.TABLE, Constants.SORT_APPLIANCES, "小家电");
        CrawlerModel kitchenware = new CrawlerModel(Constants.AMAZONSEED_KITCHENWARE, Constants.TABLE, Constants.SORT_APPLIANCES, "厨具");
        CrawlerModel home = new CrawlerModel(Constants.AMAZONSEED_HOME, Constants.TABLE, Constants.SORT_APPLIANCES, "家居");
        CrawlerModel food = new CrawlerModel(Constants.AMAZONSEED_FOOD, Constants.TABLE, Constants.SORT_FOOD, "食品");
        CrawlerModel care = new CrawlerModel(Constants.AMAZONSEED_CARE, Constants.TABLE, Constants.SORT_CARE, "美容化妆");
        CrawlerModel baby = new CrawlerModel(Constants.AMAZONSEED_BABY, Constants.TABLE, Constants.SORT_KIDS, "母婴用品");
        CrawlerModel outdoor = new CrawlerModel(Constants.AMAZONSEED_OUTDOOR, Constants.TABLE, Constants.SORT_SPORT, "户外用品");
        CrawlerModel dress = new CrawlerModel(Constants.AMAZONSEED_DRESS, Constants.TABLE, Constants.SORT_CLOTHANDSHOSE, "服装箱包");
        CrawlerModel horologe = new CrawlerModel(Constants.AMAZONSEED_HOROLOGE, Constants.TABLE, Constants.SORT_ACCESSORY, "钟表");
        CrawlerModel jewelry = new CrawlerModel(Constants.AMAZONSEED_JEWELRY, Constants.TABLE, Constants.SORT_ACCESSORY, "珠宝首饰");
        CrawlerModel gps = new CrawlerModel(Constants.AMAZONSEED_GPS, Constants.TABLE, Constants.SORT_CAR, "GPS导航仪");
        CrawlerModel car = new CrawlerModel(Constants.AMAZONSEED_CAR, Constants.TABLE, Constants.SORT_CAR, "汽车用品");

        crawlerModelList.add(phone);
        crawlerModelList.add(phoneac);
        crawlerModelList.add(cammera);
        crawlerModelList.add(dv);
        crawlerModelList.add(appliance_b);
        crawlerModelList.add(appliance_s);
        crawlerModelList.add(kitchenware);
        crawlerModelList.add(filmcammera);
        crawlerModelList.add(lens);
        crawlerModelList.add(cammeraac);
        crawlerModelList.add(digi);
        crawlerModelList.add(computer);
        crawlerModelList.add(tv);
        crawlerModelList.add(home);
        crawlerModelList.add(food);
        crawlerModelList.add(care);
        crawlerModelList.add(baby);
        crawlerModelList.add(outdoor);
        crawlerModelList.add(dress);
        crawlerModelList.add(horologe);
        crawlerModelList.add(jewelry);
        crawlerModelList.add(gps);
        crawlerModelList.add(car);
        logger.info("crawlerModelList size:"+crawlerModelList.size());
        return crawlerModelList;
    }

}

package cn.amazonspider.utils;

/**
 * 常量
 *
 * @author Mr_Tank_
 *
 */
public class Constants {
    //Number of threads,change the Numbers will change the crawler number of threads
    public static final int THREADNUM = 5;
    //table name
    public static final String TABLE="t_items";

    public static final String USERAGENT = "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0";

    //Mall number
    public static final int MALLNUM_AMAZOM = 0;

    //商品一级分类
    public static final String SORT_BOOKSDVD = "图书、音像、数字商品";
    public static final String SORT_CLOTHANDSHOSE = "服饰内衣、鞋靴童装";
    public static final String SORT_FOOD = "食品饮料、酒水、生鲜";
    public static final String SORT_IMPORTED_FOOD = "进口食品、进口牛奶";
    public static final String SORT_CARE = "美容化妆、个人护理";
    public static final String SORT_HOUSEHOLD = "家居、家具、家装、厨具";
    public static final String SORT_HEALTHCARE = "保健滋补、器械、计生";
    public static final String SORT_KIDS = "母婴、玩具乐器";
    public static final String SORT_KITHENWARE = "厨卫清洁、纸、清洁剂";
    public static final String SORT_PHONE = "手机、数码、配件";
    public static final String SORT_COMPUTER = "电脑、平板、办公设备";
    public static final String SORT_APPLIANCES = "大小家电、厨电、汽车";
    public static final String SORT_ACCESSORY = "鞋靴、箱包、钟表、珠宝";
    public static final String SORT_SPORT = "运动健康、户外用品";
    public static final String SORT_CAR = "汽车用品";
    public static final String SORT_OTHERS = "其他分类";

    //Amazon URL
    public static final String AMAZONSEED_PHONE = "http://www.amazon.cn/s/ref=sr_nr_n_0?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A664978051&bbn=2016117051&ie=UTF8&qid=1394628045&rnid=2016117051";
    public static final String AMAZONSEED_PHONE_AC = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A664978051%2Cn%3A665003051&bbn=665003051&ie=UTF8&qid=1394616970";

    public static final String AMAZONSEED_TV = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A874259051&bbn=874259051&ie=UTF8&qid=1388025214";
    //摄影/摄像
    //数码相机
    public static final String AMAZONSEED_CAMERA = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A755653051%2Cn%3A755654051&bbn=874259051&sort=-launch-date&ie=UTF8&qid=1388111695";
    //数码摄像机
    public static final String AMAZONSEED_DV = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A755653051%2Cn%3A755657051&bbn=874259051&sort=-launch-date&ie=UTF8&qid=1388111942";
    //胶片相机
    public static final String AMAZONSEED_FILMCAMERA = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A755653051%2Cn%3A113033071&bbn=874259051&sort=-launch-date&ie=UTF8&qid=1388111979";
    //镜头
    public static final String AMAZONSEED_LENS = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A755653051%2Cn%3A152323071&bbn=874259051&sort=-launch-date&ie=UTF8&qid=1388112074";
    //配件
    public static final String AMAZONSEED_CAMERA_AC = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A755653051%2Cn%3A755655051&bbn=874259051&sort=-launch-date&ie=UTF8&qid=1388112188";
    //数码影音
    public static final String AMAZONSEED_DIDG = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A760233051&bbn=2016117051&ie=UTF8&qid=1394630827";
    //电脑/IT
    public static final String AMAZONSEED_COMPURER = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A888465051&bbn=2016117051&ie=UTF8&qid=1394630885";
    //GPS导航
    public static final String AMAZONSEED_GPS = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016116051%2Cn%3A%212016117051%2Cn%3A2127789051&bbn=2016117051&ie=UTF8&qid=1394630926";
    //大家电
    public static final String AMAZONSEED_APPLIANCES_B = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A80207071&bbn=80207071&ie=UTF8&qid=1394631257";
    //小家电
    public static final String AMAZONSEED_APPLIANCES_S = "http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A814224051&bbn=814224051&ie=UTF8&qid=1394631488";
    //厨具
    public static final String AMAZONSEED_KITCHENWARE="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A813108051&ie=UTF8&qid=1394631650";
    //家居
    public static final String AMAZONSEED_HOME="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A831780051&ie=UTF8&qid=1394631793";
    //食品
    public static final String AMAZONSEED_FOOD="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2127215051&ie=UTF8&qid=1394631960";
    //美容化妆
    public static final String AMAZONSEED_CARE="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A746776051&bbn=746776051&ie=UTF8&qid=1394632012";
    //母婴用品
    public static final String AMAZONSEED_BABY="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A42692071&bbn=42692071&ie=UTF8&qid=1394632115";
    //运动户外休闲
    public static final String AMAZONSEED_OUTDOOR="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A836312051&bbn=836312051&ie=UTF8&qid=1394632246";
    //服装箱包
    public static final String AMAZONSEED_DRESS="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A2016156051&bbn=2016156051&ie=UTF8&qid=1394632336";
    //钟表
    public static final String AMAZONSEED_HOROLOGE="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A1953164051&bbn=1953164051&ie=UTF8&qid=1394632388";
    //珠宝首饰
    public static final String AMAZONSEED_JEWELRY="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A816482051&bbn=816482051&ie=UTF8&qid=1394632568";
    //汽车用品
    public static final String AMAZONSEED_CAR="http://www.amazon.cn/s/ref=sr_pg_1?rh=n%3A1947899051&bbn=1947899051&ie=UTF8&qid=1394632679";
}
